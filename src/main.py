#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from telethon import TelegramClient
from telethon.tl import types
from telethon.tl import functions
import datetime
import telethon_mods
import curses_UI

#-------------

my_client = TelegramClient(name, api_id, api_hash)

try:
    my_client.connect()
except:
    print("An error occurred while connecting.")
    exit()

if my_client.is_user_authorized()!=True:
    phone_number=str(input("Phone number: "))
    my_client.send_code_request(phone_number)
    my_login = my_client.sign_in(phone_number, str(input("Enter code: ")))
else:
    print("Logged in!\n")
    import previewers

#-------------

def sendmessage(target, message, reply=None):
    my_client.send_message(target, str(message), reply)

#-------------

def see_messages_from(username, limit=10):
    for x in range(0, limit):
        message_list = list(my_client.get_message_history(username, limit))
        message_list.reverse() #unless you want to start from oldest to newest in message_list
        message_raw = message_list[x]
        print("msgID("+str(message_raw.id)+")", #msg id (int) ex 723550
              my_client.get_entity(message_raw.from_id).first_name,"\b:", # 'name:'
              message_raw.message ) #text message content

#-------------

def search_user_raw(username):
    result=my_client.get_entity(str(username))
    if isinstance(result.status, types.UserStatusOffline):
        user_status = ("Offline; last seen "+ str(result.status.offline_info()[:2]))
    elif isinstance(result.status, types.UserStatusOnline):
        user_status = ("Online")
    elif isinstance(result.status, types.UserStatusRecently):
        user_status = ("Seen recently..")
    return(result.id,  #0
           result.first_name, #1
           result.last_name, #2
           result.username, #3
           result.contact, #4
           result.mutual_contact, #5
           result.phone, #6
           result.bot, #7
           result.access_hash, #8
           result.is_self,#9
           user_status) #10

#-------------

def search_user(username):
    s_u_r = search_user_raw(username)
    print("ID:",s_u_r[0])
    print("Username:", s_u_r[3])
    print("First name (as diplayed):", s_u_r[1])
    print("Last name (as displayed):", s_u_r[2])

    if s_u_r[4] == False:
        print("Is not in your contact list")
    else:
        print("Is in your contact list")
        print("Number:", s_u_r[6])

    if s_u_r[7] == True:
        print("Is a BOT 🤖")
    else:
        print("Is not a BOT")
    if s_u_r[9] == True:
        print("It's you!")

    print("Status:",s_u_r[10])

#-------------

def see_messages(limit=10):
    current_limit=limit
    dialogs = list(my_client.get_dialogs(limit))
    for item in dialogs:
        previewers.message_full_preview(item, my_client)


print("Current UI state:", curses_UI.UI_on)

main_process = curses_UI.UIThread(my_client)
main_process.start()
