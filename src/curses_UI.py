#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import curses
from os import system as shell
from time import sleep, strftime
from threading import Thread
from getpass import getuser
from telethon.tl import types
from telethon import TelegramClient

#-------------

class UIThread(Thread):
    def __init__(self,
        client,
        process_name = "Telegram_CLIent" ):

        Thread.__init__(self)
        self.client = client
        self.process_name = process_name


    def run(self):
        print("Starting new Telegram CLIent in 2 secs..")
        sleep(2)
        UI(self.client, pc_user=getuser(),
                        username=self.client.get_me().username,
                        name=self.client.get_me().first_name,
                        last_name=self.client.get_me().last_name,
                        user_id=self.client.get_me().id )
        curses.endwin()
        shell("clear")

#-------------

class UI_idle_process(Thread):
    def __init__(self, function, condition, delay=0):
        Thread.__init__(self)
        self.function = function
        self.condition = condition
        self.delay = delay

        if type(delay) != int:
            raise TypeError("\'delay\' must be <int>")
            curses.endwin()
            exit()

    def run(self):
        while self.condition == True:
            self.function()
            if self.delay > 0:
                sleep(self.delay)

#-------------

class curses_UI_utils:
    def get_x(target):
        return(target.getmaxyx()[1])

    def get_y(target):
        return(target.getmaxyx()[0])

#-------------

UI_on = False

#-------------

def UI(telegram_client, **kwargs):
    if isinstance(telegram_client, TelegramClient) != True:
        raise TypeError("UI's \'telegram_client\' arguments is not of type TelegramClient, but", type(telegram_client))
        curses.endwin()
        return(1)
    else:
        UI_on = True
        main_box = curses.initscr() #general screen
        main_box.border(0)
        main_box.refresh()

#-------------

        client_showbox = curses.newwin(3, 30)
        client_showbox.box()
        client_showbox.addstr(1, #y axis
                              (curses_UI_utils.get_x(client_showbox)//2)//2, #centering it on the x axis
                              " Telegram CLIent ", curses.A_REVERSE)
        client_showbox.refresh()

#-------------

        info_box = curses.newwin(6, 30,
                                 curses_UI_utils.get_y(main_box)-6, 0)
        info_box.box()
        info_box.addstr(1,1,
                        "Pc user: "+kwargs["pc_user"])
        info_box.addstr(2,1,
                        "Username: @"+kwargs["username"])
        info_box.addstr(3,1,
                        "Displayed name: \'"+
                        kwargs["name"]+
                        " "+
                        ("\b" if kwargs["last_name"] == None else kwargs["last_name"])+
                        "\'")
        info_box.refresh()

        def time_idle_function():
            if UI_on == True:
                info_box.addstr(4,1, strftime("%H:%M:%S"))
                info_box.refresh()
            else:
                exit()


#-------------

        dialog_box = curses.newwin(( curses_UI_utils.get_y(main_box)-
                                    curses_UI_utils.get_y(client_showbox)-
                                    curses_UI_utils.get_y(info_box) ), #defined height of dialog box
                                   30, # defined its width
                                   curses_UI_utils.get_y(client_showbox), 0) #position in y and x
        dialog_box.box()
        dialog_box.refresh()

#-------------

        header_box = curses.newwin(3, #y axis length
                                   (curses_UI_utils.get_x(main_box) - curses_UI_utils.get_x(client_showbox)), #x axis length
                                    0, #y axis position
                                    curses_UI_utils.get_x(client_showbox))
        header_box.box()
        header_box.addstr(1, 3, #y,x
                          "Contact", curses.A_UNDERLINE)
        header_box.refresh()

#-------------

        bar_box = curses.newwin(6, #y height
                                ( curses_UI_utils.get_x(main_box) - curses_UI_utils.get_x(info_box) ), #x width
                                 curses_UI_utils.get_y(main_box)-6,
                                 curses_UI_utils.get_x(info_box) )
        bar_box.box()
        bar_box.addstr(2, 2,
                       "/command", curses.A_BOLD)
        bar_box.refresh()

#-------------
        chat_box = curses.newwin((curses_UI_utils.get_y(dialog_box)), #y height
                                 curses_UI_utils.get_x(header_box), #x width
                                 curses_UI_utils.get_y(header_box), curses_UI_utils.get_x(dialog_box)
                                 )
        chat_box.box()
        chat_box.refresh()


        time_idle_process = UI_idle_process(time_idle_function, (UI_on==True))
        time_idle_process.start()

        main_box.getch()
        UI_on = False
        curses.endwin()
