#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from telethon import TelegramClient
from telethon.tl import types
from telethon.tl import functions


def chat_preview(target, me): # [chat name] [last_one_wh_sent_last_message]
    def last_message_from():
        if isinstance(target.entity, types.User): #if chat is from a human user
            if target.entity.contact:
                #if the sender is in your contacts
                if me.get_entity(target.message.from_id).is_self == True: #if last sender is yourself
                    sender = ("YOU",'')
                else:
                    sender = (target.entity.first_name, target.entity.last_name)

                #if sender is saved as contact, it will be displayed like '[name given to your contact, like 'Mom']'
                return('\033[4m'+str(sender[0])+'\033[0m'
                 + " " + (str(sender[1])).replace("None", ''))
            else:
                #if sender is not in your contact list, it will be displayed like '[first_name+last_name if set], [username]'
                return('\033[4m'+str(sender[0])+'\033[0m'
                             +" ["+
                              target.entity.username
                              +"]")


        if isinstance(target.entity, types.Channel): # if instead the messages come from a types.Channel dialog
            if target.message.from_id == None: # channels message don't have author id in their info
                # so if sender is a channel, sender name will be channel name
                #print("channel!")
                return(True, '')[1]
            else: #but group ones do
                return('\033[4m' + 
                    target.entity.username if target.entity.username != None else me.get_entity(target.message.from_id).username
                     + '\033[0m')


    def chat_name():
        if last_message_from() == True:
            return(str(last_message_from()[1]))
        try:
            try:
                return('\033[1m'+ "[" +
                       target.entity.username +"]"+
                       '\033[0m') #return username

            except: #if user doesn't have a username
                return('\033[1m'+ "[" +
                       target.entity.first_name +""+ target.entity.last_name
                       +"]"+ '\033[0m') #return the set name

        except: # if it's a group
            return('\033[1m'+ "["
                   +target.entity.title +"]"+
                   '\033[0m') #return the group name


    return(last_message_from(),
           chat_name())


#------------------------------------------------

def message_content_preview(target, me): # return '[unread: x] {latest message: hello}'

        def unread_builder(): # [unread: x]
            if target.unread_count > 0:
                unread_messages_count = ("[unread: "+str(target.unread_count)+"]")
                #if there are messages from the contact, it will be displayed like 'contact_name [number of unread messages]'
            else:
                unread_messages_count = "[unread: 0]"
            return(unread_messages_count)

        def latest_message_builder():    # <latest message: 'hello'>
            def container(filler, in_chat=False):
                if type(filler) != str:
                    raise TypeError("filler type is %s, expected <class str>" %type(filler) )
                    return(False)
                else:
                    if in_chat == True:
                        return(filler)
                    else:
                        return("<latest message: {0}>".format(filler))


            def latest_message_str(): # 'hello'
                def caption(raw_caption): # in case of media message
                    if type(raw_caption) != str:
                        if raw_caption == None:
                            return("[no_caption]")
                        raise TypeError("<raw_caption> is not str")
                    else:
                        if raw_caption == "":
                            return("[no_caption]")
                        elif len(raw_caption)>30:
                            return((raw_caption[:29])+" [...]")
                        else:
                            return(raw_caption)

                try:
                    latest_message_raw = target.message.message
                except AttributeError:
                    latest_message_raw = target.message.action

                if latest_message_raw != "":
                    if len(latest_message_raw)>30: #Just to do not fill the whole screen with WoT
                        return(container("\'%s\' [...]" % latest_message_raw[:29]).replace("\n","") )
                    else:
                        return(container("\'%s\'" % latest_message_raw).replace("\n",""))

                else:
                    if isinstance(target.message.media, types.MessageMediaDocument):

                        if "audio" in target.message.media.document.mime_type:
                            return(container("-Audio file-"))

                        elif "video" in target.message.media.document.mime_type:
                            return(container("-Video- %s" % caption(target.message.media.caption)) )

                        elif "pdf" in target.message.media.document.mime_type:
                            return(container("-PDF-"))

                        elif target.message.media.document.to_dict()["attributes"][1]["stickerset"]:
                            return(container("sticker"))

                        else:
                            return(container("-File-"))

                    if isinstance(target.message.media, types.MessageMediaPhoto):
                        return(container("-Photo- %s" % caption(target.message.media.caption)) )

            return(latest_message_str())

        return(unread_builder(),
               latest_message_builder())

#---------------------------------------------------------


def message_full_preview(target, me):
    print(chat_preview(target, me)[1]+ " "+ #chat name
          str(chat_preview(target, me)[0])+ #last message sender
          "-> "+
          (message_content_preview(target, me))[0], #unread count
          (message_content_preview(target, me))[1], "\n")
