from telethon.tl import types
import datetime


def offline_info(self):
    return(self.was_online.strftime("%B %d %Y"), #date of last seen online
           self.was_online.strftime("%H:%M:%S"), #hour of last seen online
           self.was_online) #raw output in datetime.datetime form

types.UserStatusOffline.offline_info = offline_info
